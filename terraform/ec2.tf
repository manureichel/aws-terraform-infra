data "aws_ami" "amazon-linux-2" {
  most_recent = true


  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }


  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}
resource "aws_instance" "web" {
  ami                    = data.aws_ami.amazon-linux-2.id
  instance_type          = var.ec2_instance_type
  subnet_id              = aws_subnet.my-public-subnet.id
  vpc_security_group_ids = [aws_security_group.manu_sg.id]

  user_data = file("../user_data.sh")

  tags = {
    Name = var.ec2_instance_name
  }
}

