variable "ec2_instance_type" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "ec2_instance_name" {
  type = string
}

variable "aws_access_key" {
  type = string
}
variable "aws_secret_key" {
  type = string
}