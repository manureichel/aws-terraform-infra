#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install nginx1.12 -y
sudo systemctl start nginx
sudo systemctl enable nginx
echo '<html><body><h1>Hi From AWS. IP: '$(curl ifconfig.me)'</h1></body></html>' | sudo tee /usr/share/nginx/html/index.html
sudo systemctl restart nginx