# Proyecto de Infraestructura en AWS con Terraform

Este proyecto utiliza Terraform para provisionar una infraestructura básica en AWS, que incluye una instancia EC2 con Nginx como servidor web y una página web.


## Estructura del Proyecto

- **Makefile**: Contiene tareas útiles para simplificar el desarrollo y la gestión de la infraestructura.

- **user_data.sh**: Script de usuario para configurar la instancia EC2 con un servidor Nginx.

- El Archivo **env/dev.tfvars** contiene variables específicas del entorno.

  ```hcl
  aws_access_key    = "YOUR_AWS_ACCESS_KEY"
  aws_secret_key    = "YOUR_AWS_SECRET_KEY"
  aws_region        = "us-east-1"
  ec2_instance_type = "t2.micro"
  ec2_instance_name = "my-ec2-instance"
  ```

- **terraform**: Directorio principal que contiene archivos de configuración de Terraform.

  - **ec2.tf**: Configuración de la instancia EC2.

  - **igw.tf**: Configuración del Internet Gateway.

  - **output.tf**: Definición de salidas útiles.

  - **provider.tf**: Configuración del proveedor de Terraform (AWS).

  - **routetable.tf**: Configuración de la tabla de rutas.

  - **sg.tf**: Configuración de los Grupos de Seguridad.

  - **subnet.tf**: Configuración de las subredes.

  - **variables.tf**: Definición de variables utilizadas en otros archivos de configuración.

  - **vpc.tf**: Configuración de la Virtual Private Cloud (VPC).

## Uso del Makefile

- **make init**: Inicializa el proyecto de Terraform.

- **make format**: Formatea los archivos de configuración de Terraform.

- **make plan**: Muestra el plan de ejecución de Terraform.

- **make apply**: Aplica los cambios definidos en los archivos de configuración de Terraform.

- **make autoapply**: Aplica automáticamente los cambios sin solicitar confirmación.

- **make destroy**: Destruye la infraestructura administrada en la nube.

## Script `user_data.sh`

El script `user_data.sh` se utiliza para configurar la instancia EC2 con Nginx y una página HTML de bienvenida.

---
